import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame as pygame
import random
from time import sleep

# === Module Level Vars ===

display = None
sprites = []

# === Classes ===

class PyGameStudioException(Exception):
    def __init__(self,value):
        self.value = value

        def __str__(self):
            return repr(self.value)


class Shape:
    def __init__(self,coord):
        self.x = coord[0]
        self.y = coord[1]
    def move_to(self,coord):
        self.x = coord[0]
        self.y = coord[1]


class Box(Shape):
    def __init__(self, position, width, height, color = (0,0,0), filled = False):
        super().__init__(position)

        self.width = width
        self.height = height
        self.color = color
        self.filled = filled

        sprites.append(self)
        self.draw()
        update_window()

    def draw(self):
        if self.filled:
            pygame.draw.rect(
                display,
                self.color,
                (self.x,self.y,self.width,self.height))
            return

        pygame.draw.rect(
            display,
            self.color,
            (self.x,self.y,self.width,self.height),
            2) #thickness

class Text(Shape):
    def __init__(self,text,position,size=36):
        super().__init__(position)
        self.text = text
        self.size = size
        self.font = pygame.font.Font('freesansbold.ttf', size)

        
        self.render = self.font.render(self.text, True, (0,0,0))
        sprites.append(self)
        update_window()
        print("Tried to render text at (",self.x,self.y,")")

    def draw(self):
        display.blit(self.render,(self.x,self.y))


class Circle(Shape):
    def __init__(self,position,radius,color = (0,0,0), filled = False):
        super().__init__(position) #its the upper left

        self.radius = radius
        self.filled = filled
        self.color = color

        sprites.append(self)
        self.draw()
        update_window()

    def draw(self):
        if self.filled:
            pygame.draw.circle(display, self.color, (self.x, self.y), self.radius)
            return

        pygame.draw.circle(display, self.color, (self.x, self.y), self.radius, 2)
        
# === Decorators ===

def no_window(func):
    def wrapper(*args):
        if display:
            raise PyGameStudioException("A window is already created!")
        return func(*args)
    return wrapper

def requires_window(func):
    def wrapper(*args):
        if not display:
            raise PyGameStudioException("A window has not been created!")
        return func(*args)
    return wrapper

# === Graphics ===

@no_window
def begin_graphics():
    global display # I expected this to work withut the global
    pygame.init()
    display = pygame.display.set_mode((640,480))
    pygame.display.set_caption(':O GASP!')
    update_window()

@requires_window
def end_graphics():
    print("Goodbye!")
    pygame.quit()
    quit()

@requires_window
def remove_from_screen(obj: Shape):
    for i,sprite in enumerate(sprites):
        if sprite is obj:
            del sprites[i]
            return
    print("Tried to delete an object but it was not found!")

@requires_window
def move_to(obj: Shape, coord):
    obj.move_to(coord)
    update_window()

@requires_window
def update_window():
    display.fill((255, 255, 255))
    for spr in sprites:
        spr.draw()
    pygame.display.update()

# === Misc Functions ===

#Taken from: gitlab.com/gctaa/gasp/blob/master/gasp/api.py#L716
def random_between(num1, num2):
    if num1 == num2:
        return num1
    elif num1 > num2:
        return random.randint(num2, num1)
    else:
        return random.randint(num1, num2)

def update_when(event_type):
    """
    'key_pressed'
    'mouse_clicked'
    'next_tick'
    """
    waiting = True
    if event_type == 'key_pressed':
        while waiting:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    return str(pygame.key.name(event.key))
